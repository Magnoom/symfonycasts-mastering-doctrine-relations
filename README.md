### Setup

If you've just downloaded the code, congratulations!!

To get it working, follow these steps:

**Download Composer dependencies**

Make sure you have [Composer installed](https://getcomposer.org/download/)
and then run:

```
composer install
```

You may alternatively need to run `php composer.phar install`, depending
on how you installed Composer.

**Database Setup**

The code comes with a `docker-compose.yaml` file, and we recommend using
Docker to boot a database container. You will still have PHP installed
locally, but you'll connect to a database inside Docker. This is optional,
but I think you'll love it!

First, make sure you have [Docker installed](https://docs.docker.com/get-docker/)
and running. To start the container, run:

```
docker-compose up -d
```

Next, build the database and execute the migrations with:

```
# "symfony console" is equivalent to "bin/console"
# but its aware of your database container
symfony console doctrine:database:create
symfony console doctrine:migrations:migrate
symfony console doctrine:fixtures:load
```

(If you get an error about "MySQL server has gone away", just wait
a few seconds and try again - the container is probably still booting).

If you do *not* want to use Docker, just make sure to start your own
database server and update the `DATABASE_URL` environment variable in
`.env` or `.env.local` before running the commands above.

**Start the Symfony web server**

You can use Nginx or Apache, but Symfony's local web server
works even better.

To install the Symfony local web server, follow
"Downloading the Symfony client" instructions found
here: https://symfony.com/download - you only need to do this
once on your system.

Then, to start the web server, open a terminal, move into the
project, and run:

```
symfony serve
```

(If this is your first time using this command, you may see an
error that you need to run `symfony server:ca:install` first).

Now check out the site at `https://localhost:8000`

Have fun!

**Optional: Webpack Encore Assets**

This app uses Webpack Encore for the CSS, JS and image files. But
to keep life simple, the final, built assets are already inside the
project. So... you don't need to do anything to get thing set up!

If you *do* want to build the Webpack Encore assets manually, you
totally can! Make sure you have [yarn](https://yarnpkg.com/lang/en/)
installed and then run:

```
yarn install
yarn encore dev --watch
```

## Have Ideas, Feedback or an Issue?

If you have suggestions or questions, please feel free to
open an issue on this repository or comment on the course
itself. We're watching both :).

## Magic

Sandra's seen a leprechaun,
Eddie touched a troll,
Laurie danced with witches once,
Charlie found some goblins' gold.
Donald heard a mermaid sing,
Susy spied an elf,
But all the magic I have known
I've had to make myself.

Shel Silverstein

## Thanks!

And as always, thanks so much for your support and letting
us do what we love!

<3 Your friends at SymfonyCasts

----------------------------------------------------------------
## 1. The Answer Entity
- To add new Entity to the project, run `symfony console make:entity` name it e.g. `Answer`.
  - content -> text -> not null, username -> string -> 255 ->not null, votes -> integer -> not null.
- Make migration. Run `symfony console make:migration`.
- Run the migration. `symfony console doctrine:migrations:migrate`.

## 2. The ManyToOne Relation
So: each answer belongs to a single question. We would normally model this in the database by adding a question_id column to the answer table that's a foreign key to the question's id.
- Add a new column `question_id` to the `answer` table. The way we've done that so far in Doctrine is by adding a property to the entity class.
  - Run `symfony console make:entity` and type Answer to modify `Answer` Entity.
    -  question -> relation -> Question -> ManyToOne -> not nullable -> two-directional relation: yes -> default answers -> orphan removal: no.
  - Make migration: `symfony console make:migration`.
  - Run migration: `symfony console doctrine:migrations:migrate`.

## 3. Saving Relations
- To set required relational object just set it, e.g. `$answer->setquestion($question);`
- To apply the fixture, run: `symfony console doctrine:fixtures:load`

## 4. Relations in Foundry (Foundry generates fixtures data)
- At first, we need a factory Class. Run `symfony console make:factory`
- To assign question to the answers we need this code in `AppFixtures.php`
```php
[AppFixtures.php]
AnswerFactory::createMany(100, [
    'question' => QuestionFactory::random()
]);
```
- But this code will assign all the answers to one question. Because this method calls once. To fix this change the code to this:
```php
[AppFixtures.php]
AnswerFactory::createMany(100, function() {
    return [
        'question' => QuestionFactory::random()
    ];
});
```
- To apply fixtures run `symfony console doctrine:fixtures:load`.
- But to make life easier we can move this question value directly into `AnswerFactory.php`. And call only
```php
[AppFixtures.php]
AnswerFactory::createMany(100);
```
```php
[AnswerFactory.php]
protected function getDefaults(): array
{
    return [
        'content' => self::faker()->text(),
        'username' => self::faker()->userName(),
        'votes' => self::faker()->numberBetween(-20, 50),
        'createdAt' => self::faker()->dateTimeBetween('-1 year'),
        'question' => QuestionFactory::random(),
    ];
}
```

## 5. Foundry: Always Pass a Factory Instance to a Relation
- Like this in `AnswerFactory.php` for question parameter:
```php
[AnswerFactory.php]
protected function getDefaults(): array
{
    return [
        'content' => self::faker()->text(),
        'username' => self::faker()->userName(),
        'votes' => self::faker()->numberBetween(-20, 50),
        'createdAt' => self::faker()->dateTimeBetween('-1 year'),
        'question' => QuestionFactory::new()->unpublished()
    ];
}
```

## 6. Fetching Relations
- To get all the answers for the question you need to autowire `AnswerRepository` to action. And then call `$answerRepository->findBy()` method.
```php
[QuestionController]
public function show(Question $question, AnswerRepository $answerRepository)
{
    if ($this->isDebug) {
        $this->logger->info('We are in debug mode!');
    }

    $answers = $answerRepository->findBy(['question' => $question]);

    return $this->render('question/show.html.twig', [
        'question' => $question,
        'answers' => $answers,
    ]);
}
```
- But there is the easier way. Just get the answers from question:
```php
[QuestionController]
public function show(Question $question)
{
    if ($this->isDebug) {
        $this->logger->info('We are in debug mode!');
    }

    $answers = $question->getAnswers();

    return $this->render('question/show.html.twig', [
        'question' => $question,
        'answers' => $answers,
    ]);
}
```
In this situation Doctrine not immediately query the answers. Doctrine doesn't query f or the answers until and unless we actually use the `$answers` property. This feature is called `LazyLoading`.

## 7. Rendering Answer Data & Saving Votes
- Make changes in `show.html.twig` that will fit the new answer's data.
- And we can remove `$answers` variable from `QuestionController->show()` method. Coz we already have answers in `$question` parameter.

## 8. Owning Vs Inverse Sides of a Relation
- In `AppFixtures.php` where we populate the DataBase we can create new answers and set them a question before flushing using `$question->addAnswer($answer1);`:
```php
public function load(ObjectManager $manager)
{
    $questions = QuestionFactory::createMany(20);

    QuestionFactory::new()
        ->unpublished()
        ->many(5)
        ->create()
    ;

    AnswerFactory::createMany(100, function() use ($questions) {
        return [
            'question' => $questions[array_rand($questions)],
        ];
    });

    $question = QuestionFactory::createOne();
    $answer1 = new Answer();
    $answer1->setContent('answer 1');
    $answer1->setUsername('weawerryan');
    $answer2 = new Answer();
    $answer2->setContent('answer 2');
    $answer2->setUsername('weawerryan');

    $question->addAnswer($answer1);
    $question->addAnswer($answer2);

    $manager->persist($answer1);
    $manager->persist($answer2);

    $manager->flush();
}
```
## 9. Relation OrderBy & fetch=EXTRA_LAZY
- We can order the fetched data by adding order in the attribute:
```php
/**
 * @ORM\OneToMany(targetEntity=Answer::class, mappedBy="question", fetch="EXTRA_LAZY")
 * @ORM\OrderBy({"createdAt" = "DESC"})
 */
private $answers;
```
- The another feature that we can add `fetch="EXTRA_LAZY"` parameter for just count data if we just count it in twig.

## 10. Filtering to Return only Approved Answers
We want to have 3 statuses for answer: waiting for approval, approved, spam. And on the site we need to display only approved answers.
- So, change the Answer Entity: `symfony console make:entity`.
  - status -> string -> 15 -> not null
- Generate the migration: `symfony console make:migration`. Execute the migration: `symfony console doctrine:migrations:migrate`.
- After code changes run `symfony console doctrine:fixtures:load`.
- See the code now =)
- In this code we have a performance problem. We query for all the answers, but display only approved. That's next...

## 11. Collection Criteria for Custom Relation Queries
- In the Entity we can use `Criteria` to solve this problem.
```php
[Question.php]
public function getApprovedAnswers(): Collection
{
    $criteria = Criteria::create()
        ->andWhere(Criteria::expr()->eq('status', Answer::STATUS_APPROVED));
    
    return $this->answers->matching($criteria);
}
```
- So, we can move Criteria condition from Question `Entity` to Answer `Repository` class:
```php
[AnswerRepository.php]
public static function createApprovedCriteria(): Criteria
{
   return Criteria::create()
       ->andWhere(Criteria::expr()->eq('status', Answer::STATUS_APPROVED));
}
```
```php
[Question.php]
public function getApprovedAnswers(): Collection
{
    $criteria = AnswerRepository::createApprovedCriteria();

    return $this->answers->matching($criteria);
}
```
- One other cool thing about this `Criteria` objects is that you can reuse it with the query builder. Let's return 10 approved answers.
```php
[AnswerRepository.php]
/**
 * @return Answer[]
 */
public function findAllApproved(int $max = 10): array
{
   return $this->createQueryBuilder('answer')
       ->addCriteria(self::createApprovedCriteria())
       ->setMaxResults($max)
       ->getQuery()
       ->getResult();
}
```
## 12. Most Popular Answers Page
- Add new method `popularAnswers()` in `AnswerController.php` and new template `answer/popularAnswers.html.twig`.
- Make some changes in templates.

## 13. The |u Filter & String Component
- Install extra features for twig: `composer require twig/string-extra`.

## 14. Joining Across a Relationship & The N + 1 Problem
- Add a `Join` to `queryBuilder` and select question when selecting answers.
```php
[AnswerRepository.php]
public function findMostPopular(): array
{
   return $this->createQueryBuilder('answer')
       ->addCriteria(self::createApprovedCriteria())
       ->orderBy('answer.votes', 'DESC')
       ->innerJoin('answer.question', 'question')
       ->addSelect('question')
       ->setMaxResults(10)
       ->getQuery()
       ->getResult();
}
```

## 15. Search, the Request Object & OR Query Logic
```php
[AnswerRepository.php]
public function findMostPopular(string $search = null): array
{
   $queryBuilder = $this->createQueryBuilder('answer')
       ->addCriteria(self::createApprovedCriteria())
       ->orderBy('answer.votes', 'DESC')
       ->innerJoin('answer.question', 'question')
       ->addSelect('question');

   if ($search !== null) {
       $queryBuilder->andWhere('answer.content LIKE :searchTerm OR question.question LIKE :searchTerm')
       ->setParameter('searchTerm', '%' . $search . '%');
   }

   return $queryBuilder
       ->setMaxResults(10)
       ->getQuery()
       ->getResult();
}
```

## 16. The 4 (2?) Possible Relation Types. Add Tags.
Here we will `tag` our questions with text descriptors.
- Let's make a `Tag` Entity: `symfony console make:entity`.
  - Tag -> name -> string -> 255 -> not nullable
  - Add `TimestampableEntity` trait
```php
[Tag.php]
class Tag
{
use TimestampableEntity;
...
}
```
- Generate the migration: `symfony console make:migration`.
- Run the migration: `symfony console doctrine:migrations:migrate`.

## 17. ManyToMany Relation
- Add `tags` property to `Question` Entity: `symfony console make:entity`.
  - Question -> tags -> relation -> Tag -> ManyToMany -> yes -> questions
- Let's generate the migration: `symfony console make:migration`. Run migration: `symfony console doctrine:migrations:migrate`.

## 18. Saving Items in a ManyToMany Relation

## 19. Handling ManyToMany in Foundry
- Use `Foundry` to create Fixture Data. Start to generate Tag Factory: `symfony console make:factory`.
- Apply some code to `AppFixtures.php`
```php
[AppFixtures.php]
public function load(ObjectManager $manager)
{
    ...
    
    $questions = QuestionFactory::createMany(20, function() {
        return [
            'tags' => TagFactory::randomRange(0, 5),
        ];
    });
    
    ...
}
```
- Run the Fixture: `symfony console doctrinefixtures:load`.

## 20. Joining Across a ManyToMany
- So... when we render the `tags` for each `question`, we have the `N+1` query problem! When we had this problem before on the `answers` page, we fixed it inside of `AnswerRepository`... by `joining` across the `question` relationship and then `selecting` the `question` data.
- In the database, we need to join from `question` to `question_tag`... and then join from `question_tag` over to `tag`. So we actually need two joins.
- But in Doctrine, we get to pretend like that join table doesn't exist: Doctrine wants us to pretend that there is a direct relationship from `question` to `tag`. What I mean is, to do the join, all we need is `->leftJoin()` - because we want to get the many tags for this question - `q.tags`, `tag`.
```php
[QuestionRepository.php]
public function findAllAskedOrderedByNewest()
{
    return $this->addIsAskedQueryBuilder()
        ->orderBy('q.askedAt', 'DESC')
        ->leftJoin('q.tags', 'tag')
        ->addSelect('tag')
        ->getQuery()
        ->getResult()
    ;
}
```

## 21. ManyToMany... with Extra Fields on the Join Table?
- Remove `ManyToMany` relationship from Question.php and Tag.php entities.
- Then add new `Join Entity` manually: `symfony console make:entity`
  - QuestionTag -> question -> relation -> Question -> ManyToOne -> not null -> yes ->questionTags -> no
  - tag -> ManyToOne -> Tag -> not null -> no
  - taggedAt -> datetime_immutable -> not null
- Add default value to `$taggedAt` property
```php
[QuestionTag.php]
public function __construct()
{
    $this->taggedAt = new \DateTimeImmutable();
}
```
- Make new migrations for these properties: `symfony console make:migration`.
- Run migration: `symfony console doctrine:migrations:migrate`.
... Failed migrations make me sad =( To be continued...

## 22. When a Migration Falls Apart
- Fixing migration: add `DATETIME DEFAULT NOW()` right after the `ADD tagged_at` in the migration.
- There is not existing KEY after running migrations first time =)
- Stash your changes `git stash`. Go to previous commit `git checkout [commit_hash]`.
- Drop and recreate Database: `symfony console doctrine:database:drop --force`, `symfony console doctrine:database:create`.
- Run migrations: `symfony console doctrine:migrations:migrate`.
- Load the fixtures: `symfony console doctrine:fixtures:load`.
- Go to your last commit: `git checkout master`, `git pull`.
- Get changes from stash: `git stash pop`.
- Apply migration: `symfony console doctrine:migrations:migrate`.
- Add new migration to add `NOT NULL` to `taggedAt` column: `symfony console make:migration`.
- Run the migration: `symfony console doctrine:migrations:migrate`.

## 23. QuestionTag Fixtures & DateTimeImmutable with Faker
- Need to create and persist `QuestionTag` object directly. Generate `Foundry` Factory for it: `symfony console make:factory`.
- Add `'question' => QuestionFactory::new()` and `'tag' => TagFactory::new(),` to return in `getDefaults()` method, convert `DateTime` to `DateTimeImmutable`:
```php
[QuestionTagFactory.php]
protected function getDefaults(): array
{
    return [
        'question' => QuestionFactory::new(),
        'tag' => TagFactory::new(),
        'taggedAt' => \DateTimeImmutable::createFromMutable(self::faker()->datetime()),
    ];
}
```
- Use it in `AppFixtures.php`
```php
[AppFixtures.php]
public function load(ObjectManager $manager)
{

    TagFactory::createMany(100);

    QuestionTagFactory::createMany(10);

    return;
    
    ...
}
```
- Apply fixtures: `symfony console doctrine:fixtures:load`.
- See that for each `Tag` it generates new `Question`:
  - `symfony console doctrine:query: sql 'select * from question_tag'`,
  - `symfony console doctrine:query:sql 'select * from question`.

Fix it next...

## 24. Doing Crazy things with Foundry & Fixtures
- To create questions and tags and the relate them to different questions see this code:
```php
[AppFixtures.php]
public function load(ObjectManager $manager)
{

    TagFactory::createMany(100);

    $questions = QuestionFactory::createMany(20, function() {
        return [
            'questionTags' => QuestionTagFactory::new(function() {
                return [
                    'tag' => TagFactory::random(),
                ];
            })->many(1, 5),
        ];
    });
    
    ...
}
```
- These complicated things only for learning =) You can do it in other way:
```php
[AppFixtures.php]
public function load(ObjectManager $manager)
{

    TagFactory::createMany(100);

    $questions = QuestionFactory::createMany(20);
    
    QuestionTagFactory::createMany(100, function() {
        return [
            'tag' => TagFactory::random(),
            'question' => QuestionFactory::random(),
        ];
    });
    
    ...
}
```

## 25. JOINing Across Multiple Relationships
- add `innerJoin` to join across `question_tag` table:
```php
[QuestionRepository.php]
public function findAllAskedOrderedByNewest()
{
    return $this->addIsAskedQueryBuilder()
        ->orderBy('q.askedAt', 'DESC')
        ->leftJoin('q.questionTags', 'question_tag')
        ->innerJoin('question_tag.tag', 'tag')
        ->addSelect(['question_tag', 'tag'])
        ->getQuery()
        ->getResult()
    ;
}
```

## 26. Pagination with Pagerfanta
- Install PagerFanta Bundle: `composer require babdev/pagerfanta-bundle`
- Install the `QueryAdapter` for Doctrine ORM: `composer require pagerfanta/doctrine-orm-adapter`.
- Use it:
```php
[QuestionController.php]
public function homepage(QuestionRepository $repository)
{
    $queryBuilder = $repository->createAskedOrderedByNewestQueryBuilder();

    $pagerfanta = new PagerFanta(
        new QueryAdapter($queryBuilder)
    );
    $pagerfanta->setMaxPerPage(5);

    return $this->render('question/homepage.html.twig', [
        'pager' => $pagerfanta,
    ]);
}
```
- Now in `homepage.html.twig` we are going to loop over `pager` instead of `questions`.
```html
{% for question in pager %}
```

## 27. Themed Pagination Links
Git: [Repo](https://github.com/BabDev/PagerfantaBundle?tab=readme-ov-file).

Documentation: [Intro](https://www.babdev.com/open-source/packages/pagerfantabundle/docs/4.x/intro), [Installation](https://www.babdev.com/open-source/packages/pagerfanta/docs/4.x/installation).
- Install PagerFanta Twig support to render pagination: `composer require pagerfanta/twig`. And use it:
```html
{{ pagerfanta(pager) }}
```
- Set the current page in `homepage()` action:
```php
[QuestionController.php]
public function homepage(QuestionRepository $repository, Request $request): Response
{
    $queryBuilder = $repository->createAskedOrderedByNewestQueryBuilder();

    $pagerfanta = new PagerFanta(
        new QueryAdapter($queryBuilder)
    );
    $pagerfanta->setMaxPerPage(5);
    $pagerfanta->setCurrentPage($request->query->get('page', 1));

    return $this->render('question/homepage.html.twig', [
        'pager' => $pagerfanta,
    ]);
}
```
- Adding `/1[2,3,4]` instead of `?page=1[2,3,4]`:
```php
public function homepage(QuestionRepository $repository, int $page = 1): Response
{
    $queryBuilder = $repository->createAskedOrderedByNewestQueryBuilder();

    $pagerfanta = new PagerFanta(
        new QueryAdapter($queryBuilder)
    );
    $pagerfanta->setMaxPerPage(5);
    $pagerfanta->setCurrentPage($page);

    return $this->render('question/homepage.html.twig', [
        'pager' => $pagerfanta,
    ]);
}
```
